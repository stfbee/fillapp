package ru.ovm.fillapp

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.SeekBar
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import com.divyanshu.colorseekbar.ColorSeekBar
import kotlinx.android.synthetic.main.fragment_fill.*
import ru.ovm.fillapp.view.FillDrawingThread
import top.defaults.colorpicker.ColorPickerPopup


class FillFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_fill, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        speed_seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                fill_view.setSpeed(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

        generate.setOnClickListener {
            val ws = width_edit.text.toString()
            val hs = height_edit.text.toString()
            if (ws.isDigitsOnly() && hs.isDigitsOnly()) {
                val w = ws.toInt()
                val h = hs.toInt()

                if (fill_view.generate(w, h)) {
                    return@setOnClickListener
                }
            }

            //todo toast
        }

        algorithm_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                fill_view.filler = when (position) {
                    1 -> FillDrawingThread.FILLER.QBASED8
                    2 -> FillDrawingThread.FILLER.SCANLINE
                    else -> FillDrawingThread.FILLER.QBASED
                }
            }
        }

        color_seek_bar.setOnColorChangeListener(object : ColorSeekBar.OnColorChangeListener {
            override fun onColorChangeListener(color: Int) {
                fill_view.nextColor = color
            }
        })
    }

}