package ru.ovm.fillapp.fillers

import android.graphics.Point

interface Filler {
    fun step()

    companion object {
        val NEIGHBOR_SET = arrayOf(
            Point(-0, +1), // ↑
            Point(+1, -0), // →
            Point(+0, -1), // ↓
            Point(-1, +0)  // ←
        )

        val NEIGHBOR_SET_8 = arrayOf(
            Point(-1, +1), // ↖
            Point(-0, +1), // ↑
            Point(+1, +1), // ↗
            Point(+1, +0), // →
            Point(+1, -1), // ↘
            Point(-0, -1), // ↓
            Point(-1, -1), // ↙
            Point(-1, -0)  // ←
        )
    }
}