package ru.ovm.fillapp.fillers

import android.graphics.Point
import ru.ovm.fillapp.fillers.Filler.Companion.NEIGHBOR_SET
import java.util.*

class QueueBased(
    x: Int, y: Int,
    private val color: Int,
    private val array: Array<IntArray>
) : Filler {

    private var pixelStack = Stack<Point>()
    private val startColor = array[x][y]

    init {
        if (startColor != color) {
            pixelStack.push(Point(x, y))
            step()
        }
    }

    override fun step() {
        if (pixelStack.empty()) return

        val newStack = Stack<Point>()

        while (pixelStack.isNotEmpty()) {
            val popped = pixelStack.pop()
            val x = popped.x
            val y = popped.y

            array[x][y] = color

            NEIGHBOR_SET.forEach { p ->
                if (x + p.x in 0 until array.size && y + p.y in 0 until array[x + p.x].size) {
                    val pixel = Point(x + p.x, y + p.y)
                    if (array[pixel.x][pixel.y] == startColor) {
                        if (!newStack.contains(pixel)) {
                            newStack.push(pixel)
                        }
                    }
                }
            }
        }
        pixelStack = newStack
    }
}