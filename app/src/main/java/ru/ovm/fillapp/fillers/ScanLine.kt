package ru.ovm.fillapp.fillers

import android.graphics.Point
import java.util.*

class ScanLine(
    x: Int, y: Int,
    private val color: Int,
    private val array: Array<IntArray>
) : Filler {

    private val queue = LinkedList<Point>()
    private val startColor = array[x][y]

    init {
        if (startColor != color) {
            queue.add(Point(x, y))
            step()
        }
    }

    override fun step() {
        if (startColor == color) return
        if (queue.isEmpty()) return

        val current = queue.pollFirst()

        var x = current.x
        val y = current.y

        while (x > 0 && array[x - 1][y] == startColor) x--

        var spanUp = false
        var spanDown = false

        while (x < array.size && array[x][y] == startColor) {
            array[x][y] = color

            if (!spanUp && y > 0 && array[x][y - 1] == startColor) {
                queue.add(Point(x, y - 1))
                spanUp = true
            } else if (spanUp && y > 0 && array[x][y - 1] != startColor) {
                spanUp = false
            }

            if (!spanDown && y < array[x].size - 1 && array[x][y + 1] == startColor) {
                queue.add(Point(x, y + 1))
                spanDown = true
            } else if (spanDown && y < array[x].size - 1 && array[x][y + 1] != startColor) {
                spanDown = false
            }
            x++
        }
    }
}