package ru.ovm.fillapp.view

import android.graphics.Canvas
import android.graphics.Paint
import android.util.Log
import android.view.SurfaceHolder
import ru.ovm.fillapp.fillers.Filler
import ru.ovm.fillapp.fillers.QueueBased
import ru.ovm.fillapp.fillers.QueueBased8Way
import ru.ovm.fillapp.fillers.ScanLine
import ru.ovm.fillapp.utils.Utils
import java.util.concurrent.ConcurrentLinkedQueue
import kotlin.random.Random


class FillDrawingThread(
    private var surfaceHolder: SurfaceHolder,
    private var wallColor: Int,
    private var emptyColor: Int
) : Thread() {
    var running: Boolean = false
    var speed = 1

    private var marginX: Float = 0f
    private var marginY: Float = 0f
    private val maxTextureSize = Utils.getMaxTextureSize()

    private var fillers = ConcurrentLinkedQueue<Filler>()

    private var prev = 0L

    private var scaleFactor = 1f

    private var array: Array<IntArray> = emptyArray()

    override fun run() {
        generate(10, 10)
        running = true

        var canvas: Canvas? = null
        val paint = Paint().also {
            it.style = Paint.Style.FILL
        }

        while (running) {
            val now = System.currentTimeMillis()
            val elapsed = now - prev

            if (elapsed > 1000 / speed) {
                Log.d(TAG, "wait time ${elapsed}ms")

                var start = System.currentTimeMillis()
                step()
                Log.d(TAG, "step time ${System.currentTimeMillis() - start}ms")

                prev = now

                try {
                    canvas = surfaceHolder.lockCanvas(null)

                    synchronized(surfaceHolder) {
                        canvas.drawColor(emptyColor)

                        start = System.currentTimeMillis()
                        for (x in 0 until width) {
                            for (y in 0 until height) {
                                paint.color = array[x][y]

                                // да, тут наверное нужно юзать drawPoint() и потом
                                // скейлить канвас, но чот у меня не завелось

                                val left = x * scaleFactor + marginX
                                val top = y * scaleFactor + marginY
                                val size = 1 * scaleFactor
                                canvas.drawRect(left, top, left + size, top + size, paint)
                            }
                        }

                        Log.w(TAG, "draw time ${System.currentTimeMillis() - start}ms")
                    }
                } finally {
                    if (canvas != null) {
                        // отрисовка выполнена. выводим результат на экран
                        surfaceHolder.unlockCanvasAndPost(canvas)
                    }
                }
            }
        }
    }

    private fun step() {
        fillers.forEach { t ->
            t.step()
        }
    }

    private var width: Int = 1
    private var height: Int = 1

    fun generate(w: Int, h: Int): Boolean {
        if (w !in 1..maxTextureSize || h !in 1..maxTextureSize) {
            return false
        }

        clear()

        array = Array(w) { IntArray(h) { emptyColor } }

        width = w
        height = h

        val canvas: Canvas = surfaceHolder.lockCanvas(null)
        if (canvas.width > 0 || canvas.height > 0) {
            scaleFactor = Math.min(canvas.width.toFloat() / width, canvas.height.toFloat() / height)
        }

        marginX = (canvas.width - (width * scaleFactor)) / 2
        marginY = (canvas.height - (height * scaleFactor)) / 2

        surfaceHolder.unlockCanvasAndPost(canvas)

        randomizePixels()

        return true
    }

    private fun clear() {
        fillers.clear()
    }

    private fun randomizePixels() {
        for (x in 0 until width) {
            for (y in 0 until height) {
                array[x][y] = if (Random.nextFloat() > 0.6) wallColor else emptyColor
            }
        }
    }

    private fun fill(
        x: Int,
        y: Int,
        color: Int,
        filler: FILLER
    ) {
        fillers.add(
            when (filler) {
                FILLER.QBASED -> QueueBased(x, y, color, array)
                FILLER.QBASED8 -> QueueBased8Way(x, y, color, array)
                FILLER.SCANLINE -> ScanLine(x, y, color, array)
            }
        )
    }

    fun onTap(
        xf: Float,
        yf: Float,
        color: Int,
        filler: FILLER
    ) {
        if ((xf - marginX).toInt() !in 0 until ((scaleFactor * width).toInt()) || (yf - marginY).toInt() !in 0 until (scaleFactor * height).toInt()) return

        fill(((xf - marginX) / scaleFactor).toInt(), ((yf - marginY) / scaleFactor).toInt(), color, filler)
    }

    companion object {
        val TAG = "FillThread"
    }

    enum class FILLER {
        QBASED, QBASED8, SCANLINE
    }
}
