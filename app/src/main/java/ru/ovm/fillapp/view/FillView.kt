package ru.ovm.fillapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import ru.ovm.fillapp.R


class FillView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    SurfaceView(context, attrs, defStyleAttr), SurfaceHolder.Callback {

    var nextColor = resources.getColor(R.color.colorAccent)
    var emptyColor = resources.getColor(R.color.colorEmpty)
    var wallColor = resources.getColor(R.color.colorWall)
    var filler: FillDrawingThread.FILLER = FillDrawingThread.FILLER.QBASED

    init {
        holder.addCallback(this)
    }

    private lateinit var drawingThread: FillDrawingThread

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {}

    override fun surfaceCreated(holder: SurfaceHolder?) {
        drawingThread = FillDrawingThread(getHolder(), wallColor, emptyColor).also { it.start() }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        var retry = true
        drawingThread.running = false
        while (retry) {
            try {
                drawingThread.join()
                retry = false
            } catch (ignored: InterruptedException) {
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        drawingThread.onTap(event.x, event.y, nextColor, filler)

        return super.onTouchEvent(event)
    }

    fun setSpeed(speed: Int) {
        drawingThread.speed = Math.max(1, speed)
    }

    fun generate(width: Int, height: Int): Boolean {
        return drawingThread.generate(width, height)
    }
}